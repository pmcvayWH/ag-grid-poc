import { useState } from 'react';
import { AgGridColumn, AgGridReact } from 'ag-grid-react';

import GoalKickCheckbox from './goal-kick-checkbox'
import AbelsonPrice from './abelson-price';

import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-alpine.css';


const App = () => {
    const [gridApi, setGridApi] = useState(null);
    const [gridColumnApi, setGridColumnApi] = useState(null);

    const onGridReady = (params) => {
        console.log(params);
        setGridApi(params.api);
        setGridColumnApi(params.columnApi);
    }

    const handleToggleColumnVisibility = (columnName) => {
        const visible = gridColumnApi.getColumn(columnName).visible
        visible ? gridColumnApi.setColumnVisible(columnName, false) : gridColumnApi.setColumnVisible(columnName, true)
    };

    const tableState = {
        rowData: [
            {
                name: 'Phil McVay',
                firstGoalScorer: '20/1',
                cardedPlayer: '4/1',
                playerSentOff: '80/1',
                shotsOnTarget: '1/8',
                header: '10',
                freeKick: null,
                manOfTheMatch: '20/1',
                assistPrice: '8/1',
                gk: true
            },
            {
                name: 'Ujval Joshi',
                firstGoalScorer: '10/1',
                cardedPlayer: '8/1',
                playerSentOff: '80/1',
                shotsOnTarget: '1/8',
                header: '10',
                freeKick: null,
                manOfTheMatch: '20/1',
                assistPrice: '8/1',
                gk: true
            },
            {
                name: 'Graham Layfield',
                firstGoalScorer: '15/1',
                cardedPlayer: '12/1',
                playerSentOff: '80/1',
                shotsOnTarget: '1/8',
                header: '10',
                freeKick: null,
                manOfTheMatch: '20/1',
                assistPrice: '8/1',
                gk: false
            },
            {
                name: 'Justyna Sokolowska',
                firstGoalScorer: '20/1',
                cardedPlayer: '4/1',
                playerSentOff: '80/1',
                shotsOnTarget: '1/8',
                header: '10',
                freeKick: null,
                manOfTheMatch: '20/1',
                assistPrice: '8/1',
                gk: false
            }
        ],
        columnDefs: [
            {
                field: 'name',
                headerName: 'Name',
                editable: true,
                filter: true,
                sortable: true,
                width: 175,
                hide: false
            },
            {
                field: 'firstGoalScorer',
                headerName: 'First Goal Scorer (Abelson)',
                editable: true,
                filter: true,
                sortable: true,
                width: 175,
                cellRendererFramework: AbelsonPrice,
                cellRendererParams: {
                    originalAbelsonPrice: '80/1'
                }
            },
            {
                field: 'cardedPlayer',
                headerName: 'Carded Player (Abelson)',
                editable: true,
                filter: true,
                sortable: true,
                width: 175
            },
            {
                field: 'playerSentOff',
                headerName: 'Player Sent Off (Abelson)',
                editable: true,
                filter: true,
                sortable: true,
                width: 175
            },
            {
                field: 'shotsOnTarget',
                headerName: 'Shots On Target',
                editable: true,
                filter: true,
                sortable: true,
                width: 175
            },
            {
                field: 'header',
                headerName: 'Header (%)',
                editable: true,
                filter: true,
                sortable: true,
                width: 175
            },
            {
                field: 'freeKick',
                headerName: 'Free Kick (%)',
                editable: true,
                filter: true,
                sortable: true,
                width: 175
            },
            {
                field: 'manOfTheMatch',
                headerName: 'Man of the Match (Frac)',
                editable: true,
                filter: true,
                sortable: true,
                width: 175
            },
            {
                field: 'assistPrice',
                headerName: 'Assist Price (Frac)',
                editable: true,
                filter: true,
                sortable: true,
                width: 175
            },
            {
                field: 'gk',
                headerName: 'GK',
                width: 100,
                cellRendererFramework: GoalKickCheckbox
            }
        ]
    }

    return (
        <div className="ag-theme-alpine" style={ { height: 400, width: 1800 } }>
            <AgGridReact
                rowData={tableState.rowData}
                columnDefs={tableState.columnDefs}
                onGridReady={onGridReady}
                getRowStyle={(params) => {
                    if (params.node.rowIndex % 2 !== 0) return { background: '#f1f1f1' }
                }}
            >
                {tableState.columnDefs.forEach(column => (
                    <AgGridColumn
                        field={column.field}
                        sortable
                        filter
                        editable
                    >
                        </AgGridColumn>
                ))}
            </AgGridReact>
            <div>
                <h2>Show/Hide Columns</h2>
                <ul>
                    {tableState.columnDefs.map(column => <li><button onClick={() => handleToggleColumnVisibility(column.field)}>{column.headerName}</button></li>)}
                </ul>
            </div>
        </div>
    );
};

export default App;
