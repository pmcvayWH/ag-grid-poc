const GoalKickCheckbox = (props) => {
    const handleSetChecked = (e) => {
        props.setValue('gk', e.target.checked)
    }

    return (
        <input
            type="checkbox"
            onChange={handleSetChecked}
            checked={props.value}
        />
    )
};

export default GoalKickCheckbox;
