const AbelsonPrice = (props) => {
    return (
        <>
            <span>{props.value}</span>
            {props.originalAbelsonPrice && <span style={{ textDecoration: 'line-through', display: 'inline-block', marginLeft: '8px' }}>{props.originalAbelsonPrice}</span>}
        </>
    )
}

export default AbelsonPrice;
